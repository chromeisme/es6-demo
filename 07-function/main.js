// 函数参数的默认值

function log(x, y = 'World') {
    console.log(x, y);
}

log(); // undefined 'World'

// 与解构赋值默认值结合使用
function fetch(url, { body = '', method = 'GET', headers = {} }) {
    console.log(method);
}

fetch('http://example.com', {}); // "GET"


// 可能出现的问题
// 写法一
function m1({x = 0, y = 0} = {}) {
    return [x, y];
}

// 写法二 没有默认值 传入的参数会替代 { x: 0, y: 0 }
function m2({x, y} = { x: 0, y: 0 }) {
    return [x, y];
}

// 作用域
// 一旦设置了参数的默认值，函数进行声明初始化时，参数会形成一个单独的作用域（context）。
// 等到初始化结束，这个作用域就会消失。这种语法行为，在不设置参数默认值时，是不会出现的。

let x = 1;

function f(x, y = x) {
    console.log(x,y);
}

f(2); // 2 2

// rest 参数
function add(...values) {
    let sum = 0;

    for (let val of values) {
        sum += val;
    }

    return sum;
}
let result = add(2, 5, 3);
console.log(result); // 10

// 严格模式


// name 属性
// 函数的name属性，返回该函数的函数名。

function foo() {}
console.log(foo.name); // "foo"


// 箭头函数

// 用“箭头”（=>）定义函数。

let sum = (num1, num2) => num1 + num2;
// 等同于
let sumOrigin = function(num1, num2) {
    return num1 + num2;
};
// 如果箭头函数的代码块部分多于一条语句，就要使用大括号将它们括起来，并且使用return语句返回。
// 由于大括号被解释为代码块，所以如果箭头函数直接返回一个对象，必须在对象外面加上括号，否则会报错。
let getTempItem = id => ({ id: id, name: "Temp" });

// 双冒号运算符
// 箭头函数并不适用于所有场合，所以现在有一个提案，提出了“函数绑定”（function bind）运算符，用来取代call、apply、bind调用。
// 函数绑定运算符是并排的两个冒号（::），双冒号左边是一个对象，右边是一个函数。


// 尾调用优化


// 函数参数的尾逗号
// ES2017 允许函数的最后一个参数有尾逗号（trailing comma）。