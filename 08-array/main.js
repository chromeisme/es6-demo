// 扩展运算符

console.log(...[1, 2, 3]); // 1 2 3

console.log(1, ...[2, 3, 4], 5); // 1 2 3 4 5

// Array.from()
// Array.of()
// Array.of方法用于将一组值，转换为数组。

Array.of(3, 11, 8) // [3,11,8]
Array.of(3) // [3]
Array.of(3).length // 1


// 数组实例的 copyWithin()
// 数组实例的 find() 和 findIndex()

// 数组实例的 fill()
new Array(3).fill(7) // [7, 7, 7]

// 数组实例的 entries()，keys() 和 values()
// 数组实例的 includes()
[1, 2, 3].includes(4)     // false
[1, 2, NaN].includes(NaN) // true

// 数组实例的 flat()，flatMap()
[1, 2, [3, 4]].flat(); // [1, 2, 3, 4]
[1, 2, [3, [4, 5]]].flat() // [1, 2, 3, [4, 5]]
[1, 2, [3, [4, 5]]].flat(2) // [1, 2, 3, 4, 5]
[1, [2, [3]]].flat(Infinity) // [1, 2, 3]

// flatMap()方法对原数组的每个成员执行一个函数（相当于执行Array.prototype.map()），然后对返回值组成的数组执行flat()方法。
// 该方法返回一个新数组，不改变原数组。

// 相当于 [[2, 4], [3, 6], [4, 8]].flat()
[2, 3, 4].flatMap((x) => [x, x * 2])
// [2, 4, 3, 6, 4, 8]

// 数组的空位

Array(3) // [, , ,]