// RegExp 构造函数
let regex = new RegExp('xyz', 'i');
// 等价于
let regex2 = /xyz/i;


// u 修饰符

/^\uD83D/.test('\uD83D\uDC2A');

// RegExp.prototype.unicode 属性

const r1 = /hello/;
const r2 = /hello/u;

console.log(r1.unicode); // false
console.log(r2.unicode); // true

// y 修饰符
// y修饰符的作用与g修饰符类似，也是全局匹配，后一次匹配都从上一次匹配成功的下一个位置开始。
// 不同之处在于，g修饰符只要剩余位置中存在匹配就可，而y修饰符确保匹配必须从剩余的第一个位置开始，这也就是“粘连”的涵义。

// RegExp.prototype.sticky 属性

let r = /hello\d/y;
console.log(r.sticky); // true

// RegExp.prototype.flags 属性

// ES5 的 source 属性
// 返回正则表达式的正文
console.log(/abc/ig.source); // "abc"

// ES6 的 flags 属性
// 返回正则表达式的修饰符
console.log(/abc/ig.flags); // 'gi'

// s 修饰符：dotAll 模式
// 后行断言
// Unicode 属性类


// 具名组匹配
// 允许为每一个组匹配指定一个名字，既便于阅读代码，又便于引用。
// const RE_DATE = /(\d{4})-(\d{2})-(\d{2})/;
const RE_DATE = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/;

const matchObj = RE_DATE.exec('1999-12-31');
const year = matchObj.groups.year; // 1999
const month = matchObj.groups.month; // 12
const day = matchObj.groups.day; // 31

console.log(year);
console.log(month);
console.log(day);

let re = /(?<year>\d{4})-(?<month>\d{2})-(?<day>\d{2})/u;
console.log('2015-01-02'.replace(re, '$<day>/$<month>/$<year>')); // '02/01/2015'

// 上面代码中，replace方法的第二个参数是一个字符串，而不是正则表达式。

// String.prototype.matchAll