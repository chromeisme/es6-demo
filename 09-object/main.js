// 属性的简洁表示法
// ES6 允许在对象之中，直接写变量。这时，属性名为变量名, 属性值为变量的值。
const foo = 'bar';
const baz = {foo};
console.log(baz); // {foo: "bar"}

// 属性名表达式
let lastWord = 'last word';

const a = {
    'first word': 'hello',
    [lastWord]: 'world'
};

console.log(a['first word']); // "hello"
console.log(a[lastWord]); // "world"
console.log(a['last word']); // "world"

// 方法的 name 属性

// 属性的可枚举性和遍历
let obj = { foo: 123 };
Object.getOwnPropertyDescriptor(obj, 'foo');

//  {
//    value: 123,
//    writable: true,
//    enumerable: true,
//    configurable: true
//  }


// for...in循环：只遍历对象自身的和继承的可枚举的属性。
// Object.keys()：返回对象自身的所有可枚举的属性的键名。
// JSON.stringify()：只串行化对象自身的可枚举的属性。
// Object.assign()： 忽略enumerable为false的属性，只拷贝对象自身的可枚举的属性。


// super 关键字
// this关键字总是指向函数所在的当前对象，ES6 又新增了另一个类似的关键字super，指向当前对象的原型对象。

const proto = {
    foo: 'hello'
};

const obj2 = {
    foo: 'world',
    find() {
        return super.foo;
    }
};

Object.setPrototypeOf(obj2, proto);
obj2.find(); // "hello"

// 对象的扩展运算符
// 对象的扩展运算符等同于使用Object.assign()方法。

let aClone = { ...a };
// 等同于
let aClone2 = Object.assign({}, a);

// let ab = { ...a, ...b };
// // 等同于
// let ab2 = Object.assign({}, a, b);



