// 对象的解构赋值

let {foo, bar} = {foo: "aaa", bar: "bbb"};
console.log(foo); // "aaa"
console.log(bar); // "bbb"

let {length : len} = 'hello';
console.log(len); // 5

// 数值和布尔值的解构赋值，会先转为对象。
let {toString:s} = 123;
console.log(s); // function toString()

