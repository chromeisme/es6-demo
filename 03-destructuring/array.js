// 数组的解构赋值

let [a, b, c] = [1, 2, 3];
console.log(a); // 1
console.log(b); // 2
console.log(c); // 3

// 跳过赋值
let [ , , third] = ["foo", "bar", "baz"];
console.log(third); // 1

