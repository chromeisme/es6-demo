// 用途

// 交换变量的值
let x = 1;
let y = 2;

[x, y] = [y, x];

// 从函数返回多个值、提取 JSON 数据
function example() {
    return [1, 2, 3];
}
let [a, b, c] = example();

