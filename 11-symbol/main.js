// ES5 的对象属性名都是字符串，这容易造成属性名的冲突。
// 如果有一种机制，保证每个属性的名字都是独一无二的就好了，这样就从根本上防止属性名的冲突。这就是 ES6 引入Symbol的原因。

// Symbol.for 找到作用域中的symbol
let s1 = Symbol.for('foo');
let s2 = Symbol.for('foo');

console.log(s1 === s2); // true

// Symbol作为函数不能使用new
// Symbol不参与运算
// Symbol可以显式转换成字符串
// Symbol可以转换成bool但是不能转换成数字。

// 实际用途

// 避免相同属性名冲突
// 模拟私有属性，由于不会被常规方法遍历到，所以可以用来隐藏属性
// Symbol 作为属性名，该属性不会出现在for...in、for...of循环中，也不会被Object.keys()、Object.getOwnPropertyNames()、JSON.stringify()返回。
// 但是，它也不是私有属性，有一个Object.getOwnPropertySymbols方法，可以获取指定对象的所有 Symbol 属性名。
let size = Symbol('size');

class Collection {
    constructor() {
        this[size] = 0;//模拟私有属性
    }
}
let foo = new Collection();
Object.getOwnPropertySymbols(foo); //Array [ Symbol(size) ]