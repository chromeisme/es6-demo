// Proxy 的核心作用是控制外界对被代理者内部的访问，Decorator 的核心作用是增强被装饰者的功能。

let validator = {
    set: function(obj, prop, value) {
        if (prop === 'age') {
            if (!Number.isInteger(value)) {
                throw new TypeError('The age is not an integer');
            }
            if (value > 200) {
                throw new RangeError('The age seems invalid');
            }
        }

        // The default behavior to store the value
        obj[prop] = value;
    }
};

let person = new Proxy({}, validator);

person.age = 100;

console.log(person.age); 

try{
    person.age = 'young';
}catch (e) {
    console.log(e.message)
}

try{
    person.age = 300;
}catch (e) {
    console.log(e.message)
}


let proxy = new Proxy({}, {
    get: function(obj, prop) {
        return 43;
    }
});
console.log(proxy.time); // 43
console.log(proxy.title); // 43