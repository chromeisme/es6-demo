// 语法提案的批准流程
// Stage 0 - Strawman（展示阶段）
// Stage 1 - Proposal（征求意见阶段）
// Stage 2 - Draft（草案阶段）
// Stage 3 - Candidate（候选人阶段）
// Stage 4 - Finished（定案阶段）
// 一个提案只要能进入 Stage 2，就差不多肯定会包括在以后的正式标准里面。ECMAScript 当前的所有提案


// Babel 转码器
// 用于将代码转换成兼容性更好的代码