// 字符的 Unicode 表示法
// codePointAt()
// String.fromCodePoint()

// 字符串的遍历器接口

// normalize()

// includes(), startsWith(), endsWith()

// repeat()

// 模板字符串
// 用反引号（`）标识。它可以当作普通字符串使用，也可以用来定义多行字符串，或者在字符串中嵌入变量。

// 如果模板字符串中的变量没有声明，将报错。
// 普通字符串
`In JavaScript '\n' is a line-feed.`

// 多行字符串
`In JavaScript this is
 not legal.`

console.log(`string text line 1
string text line 2`);

// 字符串中嵌入变量
let name = "Bob", time = "today";
`Hello ${name}, how are you ${time}?`

// 实例：模板编译

// 标签模板
alert`123`
// 等同于
alert(123)

// String.raw()
// 模板字符串的限制