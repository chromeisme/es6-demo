// 简介

class Point {
}

class ColorPoint extends Point {
}

// Object.getPrototypeOf()

console.log(Object.getPrototypeOf(ColorPoint) === Point);

// super 关键字

// super这个关键字，既可以当作函数使用，也可以当作对象使用。

class A {
    constructor() {
        console.log(new.target.name);
    }
}
class B extends A {
    constructor() {
        super();
    }
}

// 类的 prototype 属性和__proto__属性

// B.prototype.__proto__ = A.prototype;

let b = new B();
console.log(b.__proto__.__proto__ === A.prototype);
console.log(b.__proto__ === B.prototype);

// 原生构造函数的继承

class ExtendableError extends Error {
    constructor(message) {
        super();
        this.message = message;
        this.stack = (new Error()).stack;
        this.name = this.constructor.name;
    }
}


// Mixin 模式的实现

// Mixin 指的是多个对象合成一个新的对象