// 简介
class Point {
    constructor() {
        // ...
    }

    toString() {
        // ...
    }

    toValue() {
        // ...
    }
}

// 等同于

Point.prototype = {
    constructor() {},
    toString() {},
    toValue() {},
};

// 严格模式
// constructor 方法

class Point {
}

// 等同于
class Point {
    constructor() {}
}

// 类的实例对象
// Class 表达式
// 不存在变量提升
// 私有方法和私有属性
// this 的指向
class Logger {
    constructor() {
        this.printName = this.printName.bind(this);
    }

    // ...
}

class Logger {
    constructor() {
        this.printName = (name = 'there') => {
            this.print(`Hello ${name}`);
        };
    }

    // ...
}

// name 属性
// Class 的取值函数（getter）和存值函数（setter）
// Class 的 Generator 方法
// Class 的静态方法
// Class 的静态属性和实例属性
// new.target 属性 类似于ABC
class Shape {
    constructor() {
        if (new.target === Shape) {
            throw new Error('本类不能实例化');
        }
    }
}

class Rectangle extends Shape {
    constructor(length, width) {
        super();
        // ...
    }
}

var x = new Shape();  // 报错
var y = new Rectangle(3, 4);  // 正确